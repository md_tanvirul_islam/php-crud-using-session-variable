<?php session_start();?>
<html>
<head>
    <title>Index Page</title>
</head>
<body>
    <?php
    if(isset($_SESSION['status']))
    {
        echo $_SESSION['status'];
        unset($_SESSION['status']);
    }

    ?>
    <table border="1" cellpadding="10px" >
        <tr >
            <td colspan="3">To Create New User </td>
            <td colspan="2">
                <a href="create.php">Click here!</a>
            </td>

        </tr>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>
            <td>Birth Date</td>
            <td>Actions</td>
        </tr>
     <?php
     if(isset($_SESSION['user_data'])){
     $count =0;
        foreach($_SESSION['user_data'] as $key=>$user ){


     ?>
        <tr>
            <td><?php echo ++$count?></td>
            <td><?=$user["uname"]?></td>
            <td><?=$user["uemail"]?></td>
            <td><?=$user["ubirth_date"]?></td>
            <td>
                <a href="show.php?index=<?=$key?>">Show</a>
                <a href="edit.php?index=<?=$key?>">Edit</a>

                <a href='delete.php?index=<?=$key?>'>Delete</a>
            </td>
        </tr>
      <?php
      }}
     else
         echo "No Record Found";


        ?>

    </table>
</body>
</html>
